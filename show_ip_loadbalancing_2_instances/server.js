var http = require('http');
var os = require('os');
var ifaces = os.networkInterfaces();

var handleRequest = function(request, response) {
  console.log('Received request for URL: ' + request.url);
  response.writeHead(200);
  response.end(getifaces());
};
var www = http.createServer(handleRequest);
www.listen(8080);




function getifaces() {
  var txt="";
  Object.keys(ifaces).forEach(function (ifname) {
    var alias = 0;

    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return "none";
      }

      if (alias >= 1) {
        // this single interface has multiple ipv4 addresses
        txt += ifname + ' => ' + alias + iface.address+"\n ";
      } else {
        txt += ifname + " => " + iface.address+" \n ";
      }
      ++alias;
    });
  });
  return txt;
}
