# Installation kubernetes HyperV windows 10
Tuto medium 01/2019
> https://medium.com/@JockDaRock/minikube-on-windows-10-with-hyper-v-6ef0f4dc158c

 ## Installation de minikube

Installation avec choco
> choco install minikube

Fonctionnement avec hyperv
 - activer la virtualisation dans le bios
 - activer hyper v sur windows
 - Création d'un réseaux virtuel interne (branché sur la carte wifi /!\)

Créer la machine docker
> docker-machine create -d hyperv --hyperv-virtual-switch "kube_docker" default

Supprimer la machine créée
> docker-machine rm default

Lancer le cluster avec hyperV
> minikube start --vm-driver="hyperv" --hyperv-virtual-switch="kube_docker"

## Manager minkube
Lancer le dashboard minikube dans le navigateur
> minikube dashboard

Eteindre le cluster de minikube (bug connu sur windows/MacOS)
> minikube ssh | sudo poweroff

Supprimer le cluster
> minikube delete

ATTENTION: la commande minikube ne peut etre lancée que dans le terminal où le start a fonctionné. Du côté kub client osef

## Commandes basiques:
kubectl apply -f "file"
kubectl get deployments
kubectl get pods
kubectl get services
kubectl get pv //volume
kubectl get statefulsets
kubectl describe "name"
kubectl logs "name" "submodule"
kubectl exec -it "pod" -- /bin/bash // execute le bash dans un pod donné


### Command d'ajout en runtime
 - Ajout d'un service
   - kubectl expose deployment hello-node --type=LoadBalancer --port=8080


# DOCKER
create local repo
> https://stackoverflow.com/questions/42564058/how-to-use-local-docker-images-with-minikube

Concernant l'auto scalling, soucis avec les certificat autosigné sous windows/mac avec kubelet donc il faut faire
> kubectl -n kube-system edit deploy metrics-server

Ajouter la ligne en dessous de /metrics
> - --kubelet-insecure-tls

Et changer la source
> --source=kubernetes.summary_api:https://kubernetes.default?kubeletHttps=true&kubeletPort=10250&insecure=true

Puis supprimer et redeployer le deployment
