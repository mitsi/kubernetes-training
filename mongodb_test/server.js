const http = require('http')
const os = require('os')
const url = "mongodb://mongo-service/?replicaSet=rs0"
const MongoClient = require('mongodb').MongoClient
let ifaces = os.networkInterfaces()

function connectMongo(){
  return new Promise((res, rej)=>{
    MongoClient.connect(url, function(err, db) {
        if(err) {
            rej("There smtg wrong\n"+JSON.stringify(err))
            console.log('database is not connected')
        }
        else {
          db.close()
          res("Connection ok")
        }
    });
  })
}

function addEntry(ip){
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("mydb");
    var myobj = { date: new Date().toString(), ip: ip };
    dbo.collection("entry").insertOne(myobj, function(err, res) {
      if (err) throw err;
      db.close();
    });
  });
}

function getAll(){
  return new Promise((res, rej)=>{
    MongoClient.connect(url, function(err, db) {
      if (err) rej(err);
      var dbo = db.db("mydb");
      dbo.collection("entry").find({}).toArray(function(err, result) {
          db.close();
          if (err) rej("");
          console.log(JSON.stringify(result))
          res(JSON.stringify(result));
        });
    });
  })
}

function getifaces() {
  var txt=""
  Object.keys(ifaces).forEach(function (ifname) {
    var alias = 0

    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        return "none"
      }

      if (alias >= 1) {
        txt += ifname + ' => ' + alias + iface.address+"\n "
      } else {
        txt += ifname + " => " + iface.address+" \n "
      }
      ++alias
    });
  });
  return txt
}

/*--------------------- MAIN -------------------*/
let handleRequest = function(request, response) {
  console.log('Received request for URL: ' + request.url)
  let databaseConnected = "not yet";
  connectMongo()
  .then(res => {
    databaseConnected = res
    let ip = getifaces()
    addEntry(ip)
    getAll().then(res => {
      response.writeHead(200)
      response.end("response from:"+ip+"\nDatabaseConnection: "+databaseConnected+"\nGetAll :"+res)
    }).catch(err => {
      response.writeHead(500)
      response.end("response from:"+ip+"\nDatabaseConnection: "+databaseConnected+"\nGetAll :"+err)
    })
  })
  .catch(err => {databaseConnected = err})
};

let www = http.createServer(handleRequest);

console.log("Listen on 8080")
www.listen(8080)
